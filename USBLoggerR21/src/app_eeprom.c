/*
 * app_eeprom.c
 *
 * EEPROM EMULATOR
 *
 * Created: 11/6/2015 11:40:42 AM
 *  Author: jcollier
 */ 

#include <asf.h>
#include <string.h>
#include "app_eeprom.h"

// EEPROM Memory Map
//	16kB EEPROM - Size set in fuse bits
//	124 Logical Pages (due to using the sudo EEPROM driver)

//	EEPROM_PAGE_SIZE = NVMCTRL_PAGE_SIZE - EEPROM_HEADER_SIZE
//	60 = 64 - 4



void app_eeprom_init(void)
{
	struct nvm_fusebits fusebits;

	enum status_code error_code;// =  eeprom_emulator_init();
	
	do
	{
		nvm_get_fuses (&fusebits);
		fusebits.eeprom_size = NVM_EEPROM_EMULATOR_SIZE_16384;
		nvm_set_fuses (&fusebits);
		error_code = eeprom_emulator_init();
	}while (error_code == STATUS_ERR_NO_MEMORY) ;
	
	 if (error_code != STATUS_OK) {
		// Erase the emulated EEPROM memory (assume it is unformatted or irrecoverably corrupt)
		eeprom_emulator_erase_memory();
		eeprom_emulator_init();
		// Bootloader should have already done this
	}
}

uint32_t app_eeprom_read_loc(uint32_t page, uint32_t loc)
{
	uint8_t page_data[EEPROM_PAGE_SIZE];
	eeprom_emulator_read_page(page, page_data);
	
	uint32_t tempVal = *(uint32_t *)&page_data[loc];
	
	// Check to see if the address is valid
	if(tempVal >= 0xFFFFFFFF)
	{
		tempVal = 0;
		app_eeprom_write_loc(page, tempVal, loc);
	}
	return tempVal;
}

void app_eeprom_write_loc(uint32_t page, uint32_t val, uint32_t loc)
{
	uint8_t page_data[EEPROM_PAGE_SIZE];
	eeprom_emulator_read_page(page, page_data);

	page_data[loc] = val & 0xFF;
	page_data[loc + 1] = (val >> 8) & 0xFF;
	page_data[loc + 2] = (val >> 16) & 0xFF;
	page_data[loc + 3] = (val >> 24) & 0xFF;

	eeprom_emulator_write_page(page, page_data);
	eeprom_emulator_commit_page_buffer();
}


//#define EEPROM_INDEX_INIT_DAYS				0 // 0-3
//#define EEPROM_INDEX_INIT_HOURS				4 // 4-7
//#define EEPROM_INDEX_INIT_MINUTES			8 // 8-11
//#define EEPROM_INDEX_INIT_SECONDS			12 // 12-15

void app_eeprom_write_timedata(uint32_t page, uint32_t index, uint32_t days,uint32_t hours, uint32_t minutes,uint32_t seconds)
{
	uint8_t page_data[EEPROM_PAGE_SIZE];
	eeprom_emulator_read_page(page, page_data);
	
	// Day at first address
	page_data[index] =	days & 0xFF;
	page_data[index + 1] = (days >> 8) & 0xFF;
	page_data[index + 2] = (days >> 16) & 0xFF;
	page_data[index + 3] = (days >> 24) & 0xFF;
	
	// Hour at first address
	index+=4;
	page_data[index] =	hours & 0xFF;
	page_data[index + 1] = (hours >> 8) & 0xFF;
	page_data[index + 2] = (hours >> 16) & 0xFF;
	page_data[index + 3] = (hours >> 24) & 0xFF;
	
		// Hour at first address
	index+=4;
	page_data[index] =	minutes & 0xFF;
	page_data[index + 1] = (minutes >> 8) & 0xFF;
	page_data[index + 2] = (minutes >> 16) & 0xFF;
	page_data[index + 3] = (minutes >> 24) & 0xFF;
	
		// Hour at first address
	index+=4;
	page_data[index] =	seconds & 0xFF;
	page_data[index + 1] = (seconds >> 8) & 0xFF;
	page_data[index + 2] = (seconds >> 16) & 0xFF;
	page_data[index + 3] = (seconds >> 24) & 0xFF;

	eeprom_emulator_write_page(page, page_data);
	eeprom_emulator_commit_page_buffer();
}

//#define EEPROM_INDEX_INIT_DAYS				0 // 0-3
//#define EEPROM_INDEX_INIT_HOURS				4 // 4-7
//#define EEPROM_INDEX_INIT_MINUTES			8 // 8-11
//#define EEPROM_INDEX_INIT_SECONDS			12 // 12-15
bool app_eeprom_read_timedata(uint32_t page, uint32_t index, uint32_t * days,uint32_t * hours, uint32_t * minutes,uint32_t * seconds)
{
	uint8_t page_data[EEPROM_PAGE_SIZE];
	eeprom_emulator_read_page(page, page_data);
	
	*days = 0x000000FF & *(uint32_t *)&page_data[index];
	
	index+=4;
	*hours = 0x000000FF & *(uint32_t *)&page_data[index];
	
	index+=4;
	*minutes = 0x000000FF & *(uint32_t *)&page_data[index];
	
	index+=4;
	*seconds = 0x000000FF & *(uint32_t *)&page_data[index];
	
	return true;
}