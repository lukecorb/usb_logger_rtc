/*
 * app_eeprom.h
 *
 * Created: 11/6/2015 11:40:14 AM
 *  Author: jcollier
 */ 


#ifndef APP_EEPROM_H_
#define APP_EEPROM_H_

// Firmware information
#define FW_MODE_USER_KEY	0
#define FW_MODE_NETWORK_KEY	1

#define EEPROM_PAGE_GENERAL					0
#define EEPROM_INDEX_UI_INIT				0 // 00-03
#define EEPROM_INDEX_UI_SUSPEND				4 // 04-07
#define EEPROM_INDEX_GEN_ENABLE				8 // 08-11
#define EEPROM_INDEX_POLL_DAYS				12 // 12-15
#define EEPROM_INDEX_POLL_HOURS				16 // 16-19
#define EEPROM_INDEX_POLL_MINUTES			20 // 20-23
#define EEPROM_INDEX_POLL_SECONDS			24 // 24-27
#define EEPROM_INDEX_GENERAL_RESERVED		28 // 28-59

#define EEPROM_PAGE_TIMES					1
#define EEPROM_PAGE_TIMES_MAX				8
#define EEPROM_INDEX_INIT_DAYS				0 // 0-3
#define EEPROM_INDEX_INIT_HOURS				4 // 4-7
#define EEPROM_INDEX_INIT_MINUTES			8 // 8-11
#define EEPROM_INDEX_INIT_SECONDS			12 // 12-15
#define EEPROM_INDEX_SUSP_DAYS				16 // 16-19
#define EEPROM_INDEX_SUSP_HOURS				20 // 20-23
#define EEPROM_INDEX_SUSP_MINUTES			24 // 24-27
#define EEPROM_INDEX_SUSP_SECONDS			28 // 28-31
#define EEPROM_INDEX_EN_DAYS				32 // 32-35
#define EEPROM_INDEX_EN_HOURS				36 // 36-39
#define EEPROM_INDEX_EN_MINUTES				40 // 40-43
#define EEPROM_INDEX_EN_SECONDS				44 // 44-47
#define EEPROM_INDEX_TIMES_RESERVED			48 // 48-59


#define EEPROM_INDEX_INIT_START		EEPROM_INDEX_INIT_DAYS
#define EEPROM_INDEX_SUSPEND_START	EEPROM_INDEX_SUSP_DAYS
#define EEPROM_INDEX_ENABLE_START	EEPROM_INDEX_EN_DAYS

// Change this to switch between firmware modes
#define FIRMWARE_MODE	FW_MODE_USER_KEY

void app_eeprom_init(void);
uint32_t app_eeprom_read_loc(uint32_t page, uint32_t loc);
void app_eeprom_write_loc(uint32_t page, uint32_t val, uint32_t loc);
void app_eeprom_write_timedata(uint32_t page, uint32_t index, uint32_t days,uint32_t hours, uint32_t minutes,uint32_t seconds);
bool app_eeprom_read_timedata(uint32_t page, uint32_t index, uint32_t * days,uint32_t * hours, uint32_t * minutes,uint32_t * seconds);

#endif /* APP_EEPROM_H_ */
