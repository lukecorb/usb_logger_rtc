/*
 * GPIO.h
 *
 * Created: 11/23/2016 9:30:28 PM
 *  Author: dbrainard
 */ 


#ifndef GPIO_H_
#define GPIO_H_

#include <asf.h>

// Pin Definitions
#define RED_LED			PIN_PA06
#define YELLOW_LED		PIN_PA19
#define GREEN_LED		PIN_PA08

#define USER_BUTTON		PIN_PA28
#define USER_BUTTON_MUX		PINMUX_PA28A_EIC_EXTINT8
#define USER_BUTTON_EIC_LINE		8 //PIN_PA28A_EIC_EXTINT_NUM

// Control definitions
#define RED_LED_ON		0
#define RED_LED_OFF		1
#define YELLOW_LED_ON	0
#define YELLOW_LED_OFF	1
#define GREEN_LED_ON	0
#define GREEN_LED_OFF	1
#define USER_BUTTON_OFF	1
#define USER_BUTTON_ON	0

// Function declarations
void configure_port_pins(void);

#endif /* GPIO_H_ */