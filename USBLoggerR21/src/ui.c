/*
 * ui.c
 *
 * Created: 7/15/2017 3:55:00 PM
 *  Author: dbrainard
 */ 

#include <asf.h>
#include "ui.h"
#include "sysTimer.h"
#include "app_eeprom.h"
#include "rtc.h"
#include "GPIO.h"
#include "SEGGER_RTT.h"


void ui_powerdown(void)
{

}


void ui_wakeup_enable(void)
{

}

void ui_wakeup_disable(void)
{

}

void ui_wakeup(void)
{

}

void ui_process(uint16_t framenumber)
{
	static uint8_t cpt_sof = 0;
	
	// Scan process running each 40ms
	cpt_sof++;
	if (cpt_sof < 40) {
		return;
	}
	cpt_sof = 0;
}

void ui_handle_data(uint8_t *report)
{

}
