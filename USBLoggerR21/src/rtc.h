/*
 * rtc.h
 *
 * Created: 10/1/2017 10:23:49 AM
 *  Author: dbrainard
 */ 


#ifndef RTC_H_
#define RTC_H_

#include <asf.h>

void rtc_init(void);
void rtc_set_defaulttime(struct rtc_calendar_time * timeptr);

extern struct rtc_module rtc_instance;
extern bool rtcInitialized;

#endif /* RTC_H_ */