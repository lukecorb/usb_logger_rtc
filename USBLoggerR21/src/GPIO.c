/*
 * GPIO.c
 *
 * Created: 11/23/2016 9:28:13 PM
 *  Author: dbrainard
 */ 

// Includes
#include <asf.h>
#include "GPIO.h"

// Port configuration
void configure_port_pins(void)
{
	struct port_config config_port_pin;
	
	// Configure outputs
	port_get_config_defaults(&config_port_pin);
	config_port_pin.direction = PORT_PIN_DIR_OUTPUT;
	
	// Set defaults for LEDs
	port_pin_set_output_level(RED_LED, RED_LED_OFF);
	port_pin_set_config(RED_LED, &config_port_pin);
	port_pin_set_output_level(YELLOW_LED, YELLOW_LED_OFF);
	port_pin_set_config(YELLOW_LED, &config_port_pin);
	port_pin_set_output_level(GREEN_LED, GREEN_LED_OFF);
	port_pin_set_config(GREEN_LED, &config_port_pin);
	
	// Configure inputs
	port_get_config_defaults(&config_port_pin);
	config_port_pin.direction = PORT_PIN_DIR_INPUT;
	config_port_pin.input_pull = PORT_PIN_PULL_UP;
	port_pin_set_config(USER_BUTTON, &config_port_pin);
	
}
