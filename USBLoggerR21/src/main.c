/**
* \file
*
* \brief Empty user application template
*
*/

/**
* \mainpage User Application template doxygen documentation
*
* \par Empty user application template
*
* Bare minimum empty user application template
*
* \par Content
*
* -# Include the ASF header files (through asf.h)
* -# Minimal main function that starts with a call to system_init()
* -# "Insert application code here" comment
*
*/

/*
* Include header files for all drivers that have been imported from
* Atmel Software Framework (ASF).
*/
/*
* Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
*/
#include <asf.h>
#include "main.h"
#include "GPIO.h"
#include "conf_usb.h"
#include "ui.h"
#include "sysTimer.h"
#include "app_eeprom.h"
#include "rtc.h"
#include "string.h"
#include "SEGGER_RTT.h"

#define BTN_USER_IS_PRESSED() ( !port_pin_get_input_level(USER_BUTTON) )

static void RedLEDTimerHandler(SYS_Timer_t *timer); // Timer expiration callback handler
static void YellowLEDTimerHandler(SYS_Timer_t *timer); // Timer expiration callback handler
static void GreenLEDTimerHandler(SYS_Timer_t *timer); // Timer expiration callback handler
static void BlinkLEDTimerHandler(SYS_Timer_t *timer); // Timer expiration callback handler
static void rtcStartTimerHandler(SYS_Timer_t *timer); // Timer expiration callback handler
static void ledoffTimerHandler(SYS_Timer_t *timer); // Timer expiration callback handler
static void usbHoldOffHandler(SYS_Timer_t *timer);
static void usbstatTimerCallback(SYS_Timer_t *timer);
static void SuspendHoldOffCallback(SYS_Timer_t *timer);

static void usbstatTimerHandler(void);
static void hid_usb_suspend_handler(void);
static void hid_usb_generic_enable_handler(void);

static void configure_bod33(void);
static void usb_init_save_handler(void);
static bool check_failisunique(struct rtc_calendar_time * timearray, struct rtc_calendar_time * newtime, uint8_t fail_cnt);
void restore_nvram(void);
void btn_callback(void);
static void btnTimerCallback(SYS_Timer_t *timer);
void reset_nvram(void);

static volatile bool main_b_generic_enable = false;
static volatile bool redLEDReady = true;
static volatile bool yellowLEDReady = true;
static volatile bool greenLEDReady = true;
static volatile int32_t redLEDCtrl = 0;
static volatile int32_t yellowLEDCtrl = 0;
static volatile int32_t greenLEDCtrl = 0;
static struct button_t userButton;

// System timers
static SYS_Timer_t RedLEDTimer;
static SYS_Timer_t YellowLEDTimer;
static SYS_Timer_t GreenLEDTimer;
static SYS_Timer_t rtcStartTimer;
static SYS_Timer_t ledoffTimer;
static SYS_Timer_t usbstatTimer;
static SYS_Timer_t btnTimer;
static SYS_Timer_t usbHoldOff;
static SYS_Timer_t SuspendHoldOffTimer;


// Debug variables
volatile uint32_t indexDebug = 0;
volatile uint32_t suspendDebug = 0;
volatile uint32_t enableDebug = 0;
volatile struct rtc_calendar_time initTimeLog[EEPROM_SAVE_CNT];
volatile struct rtc_calendar_time suspendTimeLog[EEPROM_SAVE_CNT];
volatile struct rtc_calendar_time enableTimeLog[EEPROM_SAVE_CNT];
volatile struct rtc_calendar_time pollTimeDebug;

uint8_t initTimeLogCounts[EEPROM_SAVE_CNT]; // number of fails at each time
uint8_t suspendTimeLogCounts[EEPROM_SAVE_CNT];
uint8_t enableTimeLogCounts[EEPROM_SAVE_CNT];

// Global variables
#define RESET_HOLD_VAL 2

// Flags

static volatile uint8_t resetHoldOffCnt = 0; // set this to avoid
static volatile bool hid_usb_suspend_callback_flag = false;
static volatile bool hid_usb_enable_callback_flag = false;
static volatile bool usbInitSaveFlag = false;
static volatile bool usbstatTimerCallbackFlag = false;
static volatile bool usbHoldOffFlag = false;
static bool reset_lock = false;
uint8_t suspend_count,enable_count,init_count;


sys_event_t sysEvents;

inline void handle_events(void)
{
	if (sysEvents.all)
	{
		if(sysEvents.evt_enable)
		{
			sysEvents.evt_enable=0;
		}
		if(sysEvents.evt_suspend)
		{
			sysEvents.evt_suspend=0;
		}
		if(sysEvents.evt_usbstat)
		{
			sysEvents.evt_usbstat=0;
		}
	}
}


int main (void)
{
	uint32_t button;
	uint32_t redCount;
	uint32_t yellowCount;
	uint32_t greenCount;

	SEGGER_RTT_Init();
	SEGGER_RTT_printf(0,"-- Program Start --\r\n");

	enum system_reset_cause myRst =	system_get_reset_cause();

	if(myRst == SYSTEM_RESET_CAUSE_EXTERNAL_RESET || myRst == SYSTEM_RESET_CAUSE_SOFTWARE  )
	{
		resetHoldOffCnt = RESET_HOLD_VAL;
	}
	else if(myRst == SYSTEM_RESET_CAUSE_POR)
	{
		resetHoldOffCnt = false;
	}

	irq_initialize_vectors();
	cpu_irq_enable();

	// Config Brown Out Detect
	configure_bod33();

	// Initialize the system
	system_init();
	configure_port_pins();

	// config
	struct extint_chan_conf eint_chan_conf_sw4;
	extint_chan_get_config_defaults(&eint_chan_conf_sw4);
	eint_chan_conf_sw4.gpio_pin           = USER_BUTTON;
	eint_chan_conf_sw4.gpio_pin_mux       = USER_BUTTON_MUX;
	eint_chan_conf_sw4.detection_criteria = EXTINT_DETECT_FALLING;
	eint_chan_conf_sw4.filter_input_signal = true;
	extint_chan_set_config(USER_BUTTON_EIC_LINE, &eint_chan_conf_sw4);
	
	extint_register_callback(btn_callback,USER_BUTTON,	EXTINT_CALLBACK_TYPE_DETECT);
	extint_chan_enable_callback(USER_BUTTON,	EXTINT_CALLBACK_TYPE_DETECT);

	app_eeprom_init();
	restore_nvram();

	// Initialize timers
	SYS_TimerInit();
	RedLEDTimer.mode = SYS_TIMER_INTERVAL_MODE;
	RedLEDTimer.handler = RedLEDTimerHandler;
	YellowLEDTimer.mode = SYS_TIMER_INTERVAL_MODE;
	YellowLEDTimer.handler = YellowLEDTimerHandler;
	GreenLEDTimer.mode = SYS_TIMER_INTERVAL_MODE;
	GreenLEDTimer.handler = GreenLEDTimerHandler;
	rtcStartTimer.mode = SYS_TIMER_INTERVAL_MODE;
	rtcStartTimer.handler = rtcStartTimerHandler;
	ledoffTimer.mode = SYS_TIMER_INTERVAL_MODE;
	ledoffTimer.handler = ledoffTimerHandler;
	usbstatTimer.mode = SYS_TIMER_PERIODIC_MODE;
	usbstatTimer.handler = usbstatTimerCallback;
	usbstatTimer.interval = 2000;		// initialize timer length
	SYS_TimerStart(&usbstatTimer);	// start timer

	//ui_init();
	ui_powerdown();

	// Start USB stack to authorize VBus monitoring
	udc_start();

	rtcStartTimer.interval = 1500;		// initialize timer length
	SYS_TimerStart(&rtcStartTimer);	// start timer

	btnTimer.interval = 50;
	btnTimer.handler = btnTimerCallback;
	btnTimer.mode = SYS_TIMER_PERIODIC_MODE;
	SYS_TimerStart(&btnTimer);	// start timer

	static SYS_Timer_t BlinkAllTimer;
	BlinkAllTimer.interval = 150;
	BlinkAllTimer.handler = BlinkLEDTimerHandler;
	BlinkAllTimer.mode = SYS_TIMER_PERIODIC_MODE;

	usbHoldOff.interval = 1000*20;
	usbHoldOff.mode = SYS_TIMER_INTERVAL_MODE;
	usbHoldOff.handler = usbHoldOffHandler;
	// SYS_TimerStart(&usbHoldOff);
	usbHoldOffFlag=false;
	
	// let's hold off on logging a susped so we don't catch one on power down
	SuspendHoldOffTimer.interval = 1000;
	SuspendHoldOffTimer.handler = SuspendHoldOffCallback;
	SuspendHoldOffTimer.mode = SYS_TIMER_INTERVAL_MODE;
	
	
	struct rtc_calendar_time time;

	// Loop forever
	while (1)
	{
		// Update timers
		SYS_TimerTaskHandler();

		// Handle USB Callback flags
		hid_usb_suspend_handler();
		hid_usb_generic_enable_handler();
		usb_init_save_handler();
		usbstatTimerHandler();

		// Check for long press to reset timers

		if(userButton.debounce > (100) && !reset_lock)
		{
			//userButton.debounce=0;
			reset_lock=true;
			reset_nvram();
			// Stop blink so we know we reset the flash
			//BlinkAllTimer.interval = 1000;	// initialize timer length
			//SYS_TimerStop(&BlinkAllTimer);
			port_pin_set_output_level(GREEN_LED, GREEN_LED_OFF);
			port_pin_set_output_level(YELLOW_LED, YELLOW_LED_OFF);
			port_pin_set_output_level(RED_LED, RED_LED_OFF);

		}
		else if (userButton.pressed || userButton.debounce > 0 )
		{
			reset_lock=false;
			//BlinkAllTimer.interval = 500;	// initialize timer length
			SYS_TimerStart(&BlinkAllTimer);
			SYS_TimerStop(&RedLEDTimer);	// start timer
			SYS_TimerStop(&GreenLEDTimer);	// start timer
			SYS_TimerStop(&YellowLEDTimer);	// start timer
		}
		else
		{
			//Clear Reset Lock
			reset_lock=false;
			SYS_TimerStop(&BlinkAllTimer);
			
			// Check if Red LED should be lit

			redCount = init_count;

			if ((redCount > 0) && redLEDReady)
			{
				redLEDReady = false;

				// Turn on LED
				port_pin_set_output_level(RED_LED, RED_LED_ON);
				if (redCount < 8)
				{
					redLEDCtrl = (redCount * 2) - 2;
					RedLEDTimer.interval = 500;	// initialize timer length
					SYS_TimerStart(&RedLEDTimer);	// start timer
				}
			}
			// Check if Yellow LED should be lit
			//yellowCount = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_UI_SUSPEND);
			yellowCount=suspend_count;
			// 		yellowCount = 4;	// tests LED blinking
			if ((yellowCount > 0) && yellowLEDReady)
			{
				yellowLEDReady = false;

				// Turn on LED
				port_pin_set_output_level(YELLOW_LED, YELLOW_LED_ON);
				if (yellowCount < 8)
				{
					yellowLEDCtrl = (yellowCount * 2) - 2;
					YellowLEDTimer.interval = 500;	// initialize timer length
					SYS_TimerStart(&YellowLEDTimer);	// start timer
				}
			}
			// Check if Green LED should be lit
			//greenCount = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_GEN_ENABLE);
			greenCount = enable_count;
			// 		greenCount = 7;	// tests LED blinking
			if ((greenCount > 0) && greenLEDReady)
			{
				greenLEDReady = false;

				// Turn on LED
				port_pin_set_output_level(GREEN_LED, GREEN_LED_ON);
				if (greenCount < 8)
				{
					greenLEDCtrl = (greenCount * 2) - 2;
					GreenLEDTimer.interval = 500;	// initialize timer length
					SYS_TimerStart(&GreenLEDTimer);	// start timer
				}
			}
		}
	}																	// end while(1)

}

#if (SAMD || SAMR21)
void SYSCTRL_Handler(void)
{
	if (SYSCTRL->INTFLAG.reg & SYSCTRL_INTFLAG_BOD33DET)
	{
		SYSCTRL->INTFLAG.reg = SYSCTRL_INTFLAG_BOD33DET;
		eeprom_emulator_commit_page_buffer();
	}
}
#endif																		// end main

static void configure_bod33(void)
{
	struct bod_config config_bod33;
	bod_get_config_defaults(&config_bod33);

	config_bod33.mode = BOD_MODE_CONTINUOUS;
	config_bod33.action = BOD_ACTION_RESET;
	config_bod33.level = 42; // ~3V?
	//config_bod33.level

	bod_set_config(BOD_BOD33, &config_bod33);
	bod_enable(BOD_BOD33);
}

// Timer to handle Red LED
static void RedLEDTimerHandler(SYS_Timer_t *timer)
{
	UNUSED(timer); // To avoid compiler warning

	switch (redLEDCtrl)
	{
		case -1:
		redLEDReady = true;
		break;
		case 0:
		port_pin_set_output_level(RED_LED, RED_LED_OFF);
		redLEDCtrl--;
		RedLEDTimer.interval = 5000;	// initialize timer length
		SYS_TimerStart(&RedLEDTimer);	// start timer
		break;
		default:
		port_pin_toggle_output_level(RED_LED);
		redLEDCtrl--;
		RedLEDTimer.interval = 500;	// initialize timer length
		SYS_TimerStart(&RedLEDTimer);	// start timer
		break;
	}
}

// Timer to handle Yellow LED
static void YellowLEDTimerHandler(SYS_Timer_t *timer)
{
	UNUSED(timer); // To avoid compiler warning

	switch (yellowLEDCtrl)
	{
		case -1:
		yellowLEDReady = true;
		break;
		case 0:
		port_pin_set_output_level(YELLOW_LED, YELLOW_LED_OFF);
		yellowLEDCtrl--;
		YellowLEDTimer.interval = 5000;	// initialize timer length
		SYS_TimerStart(&YellowLEDTimer);	// start timer
		break;
		default:
		port_pin_toggle_output_level(YELLOW_LED);
		yellowLEDCtrl--;
		YellowLEDTimer.interval = 500;	// initialize timer length
		SYS_TimerStart(&YellowLEDTimer);	// start timer
		break;
	}
}

// Timer to handle Green LED
static void GreenLEDTimerHandler(SYS_Timer_t *timer)
{
	UNUSED(timer); // To avoid compiler warning

	switch (greenLEDCtrl)
	{
		case -1:
		greenLEDReady = true;
		break;
		case 0:
		port_pin_set_output_level(GREEN_LED, GREEN_LED_OFF);
		greenLEDCtrl--;
		GreenLEDTimer.interval = 5000;	// initialize timer length
		SYS_TimerStart(&GreenLEDTimer);	// start timer
		break;
		default:
		port_pin_toggle_output_level(GREEN_LED);
		greenLEDCtrl--;
		GreenLEDTimer.interval = 500;	// initialize timer length
		SYS_TimerStart(&GreenLEDTimer);	// start timer
		break;
	}
}

static void BlinkLEDTimerHandler(SYS_Timer_t *timer)
{
	UNUSED(timer); // To avoid compiler warning
	static bool blinkon = false;
	if(reset_lock)
	{
		port_pin_set_output_level(GREEN_LED, GREEN_LED_OFF);
		port_pin_set_output_level(YELLOW_LED, YELLOW_LED_OFF);
		port_pin_set_output_level(RED_LED, RED_LED_OFF);

	}
	else
	{
		if(blinkon)
		{
			blinkon=false;
			port_pin_set_output_level(GREEN_LED, GREEN_LED_OFF);
			port_pin_set_output_level(YELLOW_LED, YELLOW_LED_OFF);
			port_pin_set_output_level(RED_LED, RED_LED_OFF);
		}
		else
		{
			blinkon=true;
			port_pin_set_output_level(GREEN_LED, GREEN_LED_ON);
			port_pin_set_output_level(YELLOW_LED, YELLOW_LED_ON);
			port_pin_set_output_level(RED_LED, RED_LED_ON);
		}
		
		//port_pin_toggle_output_level(GREEN_LED);
		//port_pin_toggle_output_level(YELLOW_LED);
		//port_pin_toggle_output_level(RED_LED);

	}



}

// Timer to initialize RTC
static void rtcStartTimerHandler(SYS_Timer_t *timer)
{
	// if button pressed, do not mark init
	if ( BTN_USER_IS_PRESSED() )
	{
		usbInitSaveFlag=true;
	}
	
	// set_initial_time();
	SYS_TimerStart(&usbHoldOff);
	rtc_init();
}

// Timer to turn off LEDs from button press
static void ledoffTimerHandler(SYS_Timer_t *timer)
{
	port_pin_set_output_level(RED_LED, RED_LED_OFF);
	port_pin_set_output_level(YELLOW_LED, YELLOW_LED_OFF);
	port_pin_set_output_level(GREEN_LED, GREEN_LED_OFF);
	redLEDReady = true;
	yellowLEDReady = true;
	greenLEDReady = true;
}

#define HID_REP_OFFSET 8 // where we start storing time data

// Timer to handle usb status message
static void usbstatTimerCallback(SYS_Timer_t *timer)
{
	usbstatTimerCallbackFlag=true;
}

static void usbstatTimerHandler(void)
{
	struct rtc_calendar_time time;
	
	if(usbstatTimerCallbackFlag && rtcInitialized)
	{

		static enum report_type_t reportType = REPORT_SUSPEND;  // Cycle through each data type

		uint8_t *pnt;
		uint8_t hid_report[UDI_HID_REPORT_IN_SIZE];
		memset(hid_report,0xFF,UDI_HID_REPORT_IN_SIZE);
		uint8_t i = 0;
		
		rtc_calendar_get_time(&rtc_instance, &time);
		
		// Update time gloabl variable
			pollTimeDebug.day=time.day;
			pollTimeDebug.hour=time.hour;
			pollTimeDebug.minute=time.minute;
			pollTimeDebug.second=time.second;
		
		//for (i = 0; i < EEPROM_SAVE_CNT; i++)
		//{
			//initTimeLog[i].day = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_INIT_DAYS);
			//initTimeLog[i].hour = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_INIT_HOURS);
			//initTimeLog[i].minute = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_INIT_MINUTES);
			//initTimeLog[i].second = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_INIT_SECONDS);
//
			//suspendTimeLog[i].day = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_SUSP_DAYS);
			//suspendTimeLog[i].hour = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_SUSP_HOURS);
			//suspendTimeLog[i].minute = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_SUSP_MINUTES);
			//suspendTimeLog[i].second = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_SUSP_SECONDS);
//
			//enableTimeLog[i].day = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_EN_DAYS);
			//enableTimeLog[i].hour = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_EN_HOURS);
			//enableTimeLog[i].minute = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_EN_MINUTES);
			//enableTimeLog[i].second = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_EN_SECONDS);
		//}
		//pollTimeDebug.day = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_POLL_DAYS);
		//pollTimeDebug.hour = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_POLL_HOURS);
		//pollTimeDebug.minute = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_POLL_MINUTES);
		//pollTimeDebug.second = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_POLL_SECONDS);
		
		// Store data in the message
		hid_report[0] = init_count;
		hid_report[1] = suspend_count;
		hid_report[2] = enable_count;

		volatile struct rtc_calendar_time * temptimeptr = suspendTimeLog;
		uint32_t count = 0;

		if(reportType == REPORT_INIT)
		{
			hid_report[3] = REPORT_INIT;
			count = init_count;
			temptimeptr=initTimeLog;
		}
		else if(reportType == REPORT_HIDEN)
		{
			hid_report[3] = REPORT_HIDEN;
			count = enable_count;
			temptimeptr=enableTimeLog;
		}
		else if (reportType == REPORT_SUSPEND)
		{
			hid_report[3] = REPORT_SUSPEND;
			count = suspend_count;
			temptimeptr=suspendTimeLog;
		}

		// Loop through time structs and fill hid data with them
		// exit if we do not have any more fails for this category
		for(uint8_t tidx = 0; tidx < EEPROM_SAVE_CNT && (tidx < count); tidx++)
		{
			hid_report[HID_REP_OFFSET + tidx*4] = temptimeptr[tidx].day;
			hid_report[HID_REP_OFFSET + tidx*4 + 1] = temptimeptr[tidx].hour;
			hid_report[HID_REP_OFFSET + tidx*4 + 2] = temptimeptr[tidx].minute;
			hid_report[HID_REP_OFFSET + tidx*4 + 3] = temptimeptr[tidx].second;

		}

		// Set poll time 
		hid_report[4]=pollTimeDebug.day;
		hid_report[5]=pollTimeDebug.hour;
		hid_report[6]=pollTimeDebug.minute;
		hid_report[7]=pollTimeDebug.second;

		if(++reportType > REPORT_HIDEN )
		{
			usbstatTimer.interval = 2000;		// initialize timer length
			SYS_TimerRestart(&usbstatTimer);	// start timer
			reportType = REPORT_SUSPEND;
		}
		else
		{
			usbstatTimer.interval = 2000;		// initialize timer length
			SYS_TimerRestart(&usbstatTimer);	// start timer
		}

		// Send the message
		udi_hid_generic_send_report_in(hid_report);

		usbstatTimerCallbackFlag =false;
	}

}

void  hid_usb_suspend_callback(void)
{
	if(usbHoldOffFlag)
	{
		SYS_TimerStart(&SuspendHoldOffTimer);
	}
}

void hid_usb_suspend_handler(void)
{
	if(hid_usb_suspend_callback_flag)
	{

		struct rtc_calendar_time time;
		//uint32_t hour,minute,second,day;

		if (rtcInitialized == true && usbHoldOffFlag)
		{
			// Get current time
			rtc_calendar_get_time(&rtc_instance, &time);

			//app_eeprom_read_timedata(suspend_count,EEPROM_INDEX_SUSPEND_START,&day,&hour,&minute,&second);

			if(check_failisunique((struct rtc_calendar_time *)suspendTimeLog,&time,suspend_count) == false)
			{
				return;
			}

			//SEGGER_RTT_printf(0,"USB Suspend Fail - CNT=%d\r\n",suspend_count);
			//SEGGER_RTT_printf(0,"\tPrev %02d:%02d:%02d\r\n",hour,minute,second);
			//SEGGER_RTT_printf(0,"\tCurr %02d:%02d:%02d\r\n",time.hour,time.minute,time.second);

			//Increment Count address
			if (++suspend_count >= EEPROM_SAVE_CNT)
			{
				suspend_count = EEPROM_SAVE_CNT; // keep saving last error
			}

			app_eeprom_write_loc(EEPROM_PAGE_GENERAL, suspend_count, EEPROM_INDEX_UI_SUSPEND);

			// Store time in EEPROM
			app_eeprom_write_timedata(suspend_count,EEPROM_INDEX_SUSPEND_START,time.day,time.hour,time.minute,time.second);

			// Update time gloabl variable
			suspendTimeLog[suspend_count-1].day=time.day;
			suspendTimeLog[suspend_count-1].hour=time.hour;
			suspendTimeLog[suspend_count-1].minute=time.minute;
			suspendTimeLog[suspend_count-1].second=time.second;

			hid_usb_suspend_callback_flag=false;
		}

	}

}


void main_resume_action(void)
{
	ui_wakeup();
}

void main_sof_action(void)
{
	if (!main_b_generic_enable)
	return;
	ui_process(udd_get_frame_number());
}

void main_remotewakeup_enable(void)
{
	ui_wakeup_enable();
}

void main_remotewakeup_disable(void)
{
	ui_wakeup_disable();
}

void  main_usb_vbus_high(bool pinasserted)
{
	if(pinasserted)
	{
		usbInitSaveFlag=true;
	}
	
}


//void delcycles(uint32_t cycles)
//{
//while(--cycles)
//{
//nop();
//nop();
//nop();
//nop();
//}
//}

void hid_usb_generic_enable_handler(void)
{
	if(hid_usb_enable_callback_flag)
	{
		struct rtc_calendar_time time;
		//uint32_t hour,minute,second,day;

		if (rtcInitialized == true && usbHoldOffFlag)
		{
			// Get current time
			rtc_calendar_get_time(&rtc_instance, &time);

			//app_eeprom_read_timedata(enable_count,EEPROM_INDEX_ENABLE_START,&day,&hour,&minute,&second);

			if(check_failisunique((struct rtc_calendar_time *)enableTimeLog,&time,enable_count) == false)
			{
				return;
			}

			//SEGGER_RTT_printf(0,"HID Enable - CNT=%d\r\n",enable_count);
			//SEGGER_RTT_printf(0,"\tPrev %02d:%02d:%02d\r\n",hour,minute,second);
			//SEGGER_RTT_printf(0,"\tCurr %02d:%02d:%02d\r\n",time.hour,time.minute,time.second);
			//
			//if(day==time.day && hour==time.hour && minute==time.minute && second==time.second )
			//{
			//return ;
			//}

			if (++enable_count >= EEPROM_SAVE_CNT)
			{
				enable_count = EEPROM_SAVE_CNT; // keep saving last error
			}

			app_eeprom_write_loc(EEPROM_PAGE_GENERAL, enable_count, EEPROM_INDEX_GEN_ENABLE);

			// Store time in EEPROM
			app_eeprom_write_timedata(enable_count,EEPROM_INDEX_ENABLE_START,time.day,time.hour,time.minute,time.second);

			// Update time gloabl variable
			enableTimeLog[enable_count-1].day=time.day;
			enableTimeLog[enable_count-1].hour=time.hour;
			enableTimeLog[enable_count-1].minute=time.minute;
			enableTimeLog[enable_count-1].second=time.second;

			main_b_generic_enable = true;
			hid_usb_enable_callback_flag=false;
		}
	}

}

bool hid_usb_generic_enable_callback(void)
{
	if(usbHoldOffFlag)
	{
		hid_usb_enable_callback_flag = true;
	}
	

	return true;
}

void main_generic_disable(void)
{
	main_b_generic_enable = false;
}

void main_hid_set_feature(uint8_t* report)
{
	if (report[0] == 0xAA && report[1] == 0x55
	&& report[2] == 0xAA && report[3] == 0x55) {
		// Disconnect USB Device
		udc_stop();
		ui_powerdown();
	}
}

void usb_init_save_handler(void)
{
	struct rtc_calendar_time time;
	//uint32_t hour,minute,second,day;

	if(usbInitSaveFlag) //Flag set, we found an event
	{
		if (rtcInitialized == true) // Make sure our RTC is ready
		{
			// Get the current time
			rtc_calendar_get_time(&rtc_instance, &time);

			if(check_failisunique((struct rtc_calendar_time *)initTimeLog,&time,init_count) == false)
			{
				return;
			}

			if (++init_count >= EEPROM_SAVE_CNT)
			{
				init_count = EEPROM_SAVE_CNT; // keep saving last error
			}

			// Store Err Count in NVRAM
			app_eeprom_write_loc(EEPROM_PAGE_GENERAL, init_count, EEPROM_INDEX_UI_INIT);

			// Store time in EEPROM
			app_eeprom_write_timedata(init_count,EEPROM_INDEX_INIT_START,time.day,time.hour,time.minute,time.second);

			// Update time gloabl variable
			initTimeLog[init_count-1].day=time.day;
			initTimeLog[init_count-1].hour=time.hour;
			initTimeLog[init_count-1].minute=time.minute;
			initTimeLog[init_count-1].second=time.second;

			usbInitSaveFlag=false;
		}

	}
}


void copy_time(struct rtc_calendar_time * timeArrayElement, struct rtc_calendar_time * newtime)
{

}
// Checks if time fail is unique
static bool check_failisunique(struct rtc_calendar_time * timearray, struct rtc_calendar_time * newtime, uint8_t fail_cnt)
{
	for (int i = 0; i < EEPROM_SAVE_CNT ; i++)
	{
		if(newtime->day==timearray[i].day &&
		newtime->hour ==timearray[i].hour &&
		newtime->minute==timearray[i].minute &&
		newtime->second==timearray[i].second
		)
		{
			return false;
		}

	}
	return true; // unique time

}

void restore_nvram(void)
{

	// Set fail ram data
	init_count = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_UI_INIT);
	suspend_count = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_UI_SUSPEND);
	enable_count = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_GEN_ENABLE);

	for (int i = 0; i < EEPROM_SAVE_CNT ; i++)
	{
		initTimeLog[i].day = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_INIT_DAYS);
		initTimeLog[i].hour = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_INIT_HOURS);
		initTimeLog[i].minute = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_INIT_MINUTES);
		initTimeLog[i].second = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_INIT_SECONDS);

		suspendTimeLog[i].day = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_SUSP_DAYS);
		suspendTimeLog[i].hour = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_SUSP_HOURS);
		suspendTimeLog[i].minute = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_SUSP_MINUTES);
		suspendTimeLog[i].second = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_SUSP_SECONDS);

		enableTimeLog[i].day = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_EN_DAYS);
		enableTimeLog[i].hour = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_EN_HOURS);
		enableTimeLog[i].minute = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_EN_MINUTES);
		enableTimeLog[i].second = app_eeprom_read_loc(EEPROM_PAGE_TIMES + i, EEPROM_INDEX_EN_SECONDS);
	}
	pollTimeDebug.day = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_POLL_DAYS);
	pollTimeDebug.hour = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_POLL_HOURS);
	pollTimeDebug.minute = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_POLL_MINUTES);
	pollTimeDebug.second = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_POLL_SECONDS);

}


static void btnTimerCallback(SYS_Timer_t *timer) {

	if ( BTN_USER_IS_PRESSED() )
	{
		if(userButton.debounce < 50000)
		userButton.debounce++;
	}
	else
	{
		if(userButton.pressed || userButton.debounce>2)
		{
			port_pin_set_output_level(GREEN_LED, GREEN_LED_OFF);
			port_pin_set_output_level(YELLOW_LED, YELLOW_LED_OFF);
			port_pin_set_output_level(RED_LED, RED_LED_OFF);
			
			SYS_TimerStart(&RedLEDTimer);	// start timer
			SYS_TimerStart(&GreenLEDTimer);	// start timer
			SYS_TimerStart(&YellowLEDTimer);	// start timer
		}
		userButton.debounce=0;
		userButton.pressed=false;
	}

}


// call back function
void btn_callback(void) {

	userButton.pressed=true;

}

void reset_nvram(void)
{

	struct rtc_calendar_time time;

	eeprom_emulator_erase_memory();

	// reset RTC

	rtc_calendar_get_time_defaults(&time);
	rtc_set_defaulttime(&time);
	rtc_calendar_set_time(&rtc_instance, &time);

	// Restart RTC Clock
	app_eeprom_write_loc(EEPROM_PAGE_GENERAL, 0, EEPROM_INDEX_UI_INIT);
	app_eeprom_write_loc(EEPROM_PAGE_GENERAL, 0, EEPROM_INDEX_UI_SUSPEND);
	app_eeprom_write_loc(EEPROM_PAGE_GENERAL, 0, EEPROM_INDEX_GEN_ENABLE);
	app_eeprom_write_loc(EEPROM_PAGE_GENERAL, 1, EEPROM_INDEX_POLL_DAYS);
	app_eeprom_write_loc(EEPROM_PAGE_GENERAL, 0, EEPROM_INDEX_POLL_HOURS);
	app_eeprom_write_loc(EEPROM_PAGE_GENERAL, 0, EEPROM_INDEX_POLL_MINUTES);
	app_eeprom_write_loc(EEPROM_PAGE_GENERAL, 0, EEPROM_INDEX_POLL_SECONDS);

	enable_count=0;
	suspend_count=0;
	init_count=0;

	// Turn on LEDs to confirm
	port_pin_set_output_level(RED_LED, RED_LED_ON);
	port_pin_set_output_level(YELLOW_LED, YELLOW_LED_ON);
	port_pin_set_output_level(GREEN_LED, GREEN_LED_ON);
	ledoffTimer.interval = 500;		// initialize timer length
	SYS_TimerStart(&ledoffTimer);	// start timer
	
	yellowLEDCtrl=0;
	redLEDCtrl=0;
	greenLEDCtrl=0;


}

static void usbHoldOffHandler(SYS_Timer_t *timer) {

	usbHoldOffFlag =true;

}


static void SuspendHoldOffCallback(SYS_Timer_t *timer) {

	//usbHoldOffFlag =true;
	hid_usb_suspend_callback_flag=true;
	SYS_TimerStop(&SuspendHoldOffTimer);
}

