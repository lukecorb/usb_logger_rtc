/**
 * @file hw_timer.h
 *
 */

#ifndef HW_TIMER_H
#define HW_TIMER_H

#include <stdint.h>
#include "conf_timer.h"


void hw_timer_init(void);
uint8_t hw_timer_save_cpu_interrupt(void);
void hw_timer_restore_cpu_interrupt(uint8_t flags);


#endif /* HW_TIMER_H */
