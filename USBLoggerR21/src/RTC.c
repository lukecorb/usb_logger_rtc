/*
 * rtc.c
 *
 * Created: 10/1/2017 10:23:25 AM
 *  Author: dbrainard
 */ 

#include <asf.h>
#include "rtc.h"
#include "app_eeprom.h"
#include "SEGGER_RTT.h"

#define RTC_ALARM_INC_MINUTES 0 // Changed to allow for more time stamping
#define RTC_ALARM_INC_SECONDS 30 // Changed to allow for more time stamping

void configure_rtc_calendar(void);
void rtc_match_callback(void);
void configure_rtc_callbacks(void);
void rtc_increment_alarm(struct rtc_calendar_time * timeptr);

struct rtc_module rtc_instance;
struct rtc_calendar_alarm_time alarm;
bool rtcInitialized = false;
uint8_t suspend_count,enable_count,init_count;
extern volatile struct rtc_calendar_time pollTimeDebug;

void configure_rtc_calendar(void)
{
	/* Initialize RTC in calendar mode. */
	struct rtc_calendar_config config_rtc_calendar;
	rtc_calendar_get_config_defaults(&config_rtc_calendar);
	alarm.time.year      = 2020;
	alarm.time.month     = 1;
	alarm.time.day       = 1;
	alarm.time.hour      = 3;
	alarm.time.minute    = 1;
	alarm.time.second    = 0;
	config_rtc_calendar.clock_24h = true;
	config_rtc_calendar.alarm[0].time = alarm.time;
	config_rtc_calendar.alarm[0].mask = RTC_CALENDAR_ALARM_MASK_YEAR;
	rtc_calendar_init(&rtc_instance, RTC, &config_rtc_calendar);
	rtc_calendar_enable(&rtc_instance);
}

void rtc_match_callback(void)
{
	/* Do something on RTC alarm match here */
	// Store current time every 30 minutes
    struct rtc_calendar_time time;
	rtc_calendar_get_time(&rtc_instance, &time);
	app_eeprom_write_loc(EEPROM_PAGE_GENERAL, time.day, EEPROM_INDEX_POLL_DAYS);
	app_eeprom_write_loc(EEPROM_PAGE_GENERAL, time.hour, EEPROM_INDEX_POLL_HOURS);
	app_eeprom_write_loc(EEPROM_PAGE_GENERAL, time.minute, EEPROM_INDEX_POLL_MINUTES);
	app_eeprom_write_loc(EEPROM_PAGE_GENERAL, time.second, EEPROM_INDEX_POLL_SECONDS);
	
	rtc_increment_alarm(&time);

	rtc_calendar_set_alarm(&rtc_instance, &alarm, RTC_CALENDAR_ALARM_0);
}

void configure_rtc_callbacks(void)
{
	rtc_calendar_register_callback(
	&rtc_instance, rtc_match_callback, RTC_CALENDAR_CALLBACK_ALARM_0);
	rtc_calendar_enable_callback(&rtc_instance, RTC_CALENDAR_CALLBACK_ALARM_0);
}

void rtc_set_defaulttime(struct rtc_calendar_time * timeptr)
{
	struct rtc_calendar_time time;
	rtc_calendar_get_time_defaults(&time); // set to default

	time.day    = 1;
	time.hour   = 0;
	time.minute = 0;
	time.second = 1;
	time.year   = 2020;
	time.month  = 1;
	
	*timeptr = time;
	
	rtc_increment_alarm(timeptr);
	
	pollTimeDebug.day=time.day;
	pollTimeDebug.hour=time.hour;
	pollTimeDebug.minute=time.minute;
	pollTimeDebug.second=time.second;
	
}

void rtc_increment_alarm(struct rtc_calendar_time * timeptr)
{
	
	alarm.time = *timeptr; // copy current time
		
	/* Set new alarm in 30 minutes */
	alarm.mask = RTC_CALENDAR_ALARM_MASK_HOUR;
	
	// Increment Alarm Time Seconds
	alarm.time.second += RTC_ALARM_INC_SECONDS -1;
		
	if(alarm.time.second >=60)
	{
		alarm.time.minute++;
	}
	
	if( alarm.time.minute >= 60)
	{
		alarm.time.minute=0;
		alarm.time.hour += 1;
		if (alarm.time.hour >= 24)
		{
			alarm.time.hour=0;
			alarm.time.day++;
		}
	}
	
	
	
}


void rtc_init(void)
{
    struct rtc_calendar_time time;
    rtc_calendar_get_time_defaults(&time);
	
	// Read EEPROM Data, update with this
	
	time.day = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_POLL_DAYS);
	time.hour = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_POLL_HOURS);
	time.minute = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_POLL_MINUTES);
	time.second = app_eeprom_read_loc(EEPROM_PAGE_GENERAL, EEPROM_INDEX_POLL_SECONDS);
	
	// Check if EEPROM Data is out of spec, set to defaults
	if(time.hour>24 || time.day >31 || time.minute > 60 || time.second > 60)
	{
		rtc_set_defaulttime(&time);
	}
	
	time.year   = 2020;
	time.month  = 1;
	
	SEGGER_RTT_printf(0,"RTC is %02d:%02d:%02d\r\n",time.hour,time.minute,time.second);
	
    /* Configure and enable RTC */
    //configure_rtc_calendar();.
	
	/* Initialize RTC in calendar mode. */
	struct rtc_calendar_config config_rtc_calendar;
	rtc_calendar_get_config_defaults(&config_rtc_calendar);
	alarm.time.year      = time.year;
	alarm.time.month     = time.month;
	alarm.time.day       = time.day;
	alarm.time.hour      = time.hour;
	alarm.time.minute    = time.minute;
	alarm.time.second    = time.second;
	config_rtc_calendar.clock_24h = true;
	config_rtc_calendar.alarm[0].time = alarm.time;
	config_rtc_calendar.alarm[0].mask = RTC_CALENDAR_ALARM_MASK_YEAR;
	
	//// Alarm RTC_ALARM_MINUTES from now, check if hours increases
	//alarm.time.minute += RTC_ALARM_MINUTES;
	//if( alarm.time.minute >= 60)
	//{
		//alarm.time.minute=0;
		//alarm.time.hour += 1;
		//if (alarm.time.hour >= 24)
		//{
			//alarm.time.hour=0;
			//alarm.time.day++;
		//}
	//}
	
	
	rtc_calendar_init(&rtc_instance, RTC, &config_rtc_calendar);
	rtc_calendar_enable(&rtc_instance);
	
    /* Configure and enable callback */
    configure_rtc_callbacks();
	
	// Increment Alarm
	rtc_increment_alarm(&time);
	rtc_calendar_set_alarm(&rtc_instance, &alarm, RTC_CALENDAR_ALARM_0);
	
    /* Set current time. */
    rtc_calendar_set_time(&rtc_instance, &time);
	
	rtcInitialized = true;
}
